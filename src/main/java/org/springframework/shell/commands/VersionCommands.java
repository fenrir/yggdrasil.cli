package org.springframework.shell.commands;

import org.fenrir.yggdrasil.cli.commands.CommandMarker;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.plugin.PluginUtils;

/**
 * Essential built-in shell commands.
 * 
 * @author Mark Pollack
 * @author Erwin Vervaet
 */
public class VersionCommands implements CommandMarker 
{
	@CliCommand(value = { "version" }, help = "Displays shell version")
	public String version() 
	{
		return PluginUtils.getHighestPriorityBannerProvider().getVersion();
	}
}