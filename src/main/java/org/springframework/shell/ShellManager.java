package org.springframework.shell;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.shell.core.ExitShellRequest;
import org.springframework.shell.core.JLineShellComponent;
import org.springframework.shell.core.Shell;
import org.springframework.shell.support.logging.HandlerUtils;
import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO Documentació
 * @author aarchilla
 * @version 0.1.20141220
 */
public class ShellManager 
{
	private static final Logger log = LoggerFactory.getLogger(ShellManager.class);
	
	private static ShellManager instance;
	
	private ShellManager()
	{
		
	}
	
	public static ShellManager getInstance()
	{
		if(instance==null){
			instance = new ShellManager();
			ApplicationContext applicationContext = ApplicationContext.getInstance();
            applicationContext.injectMembers(instance);
		}
		
		return instance;
	}
	
	public void initialize(AbstractApplication application)
    {
		
    }
	
	public JLineShellComponent getJLineShellComponent() 
	{
		return (JLineShellComponent)ApplicationContext.getInstance().getRegisteredComponent(Shell.class);
	}
	
	public void createShell() throws ApplicationException
	{
		try {
			CommandLine commandLine = SimpleShellCommandLineOptions.parseCommandLine(null);
			JLineShellComponent shell = (JLineShellComponent)ApplicationContext.getInstance().getRegisteredComponent(Shell.class);
			shell.setCommandLine(commandLine);
		}
		catch(IOException e){
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
	public void runShell()
	{
		ExitShellRequest exitShellRequest;
		try{
			// The shell is used
			JLineShellComponent shell = (JLineShellComponent)ApplicationContext.getInstance().getRegisteredComponent(Shell.class);
			shell.start();
			shell.promptLoop();
			exitShellRequest = shell.getExitShellRequest();
			if (exitShellRequest == null) {
				// shouldn't really happen, but we'll fallback to this anyway
				exitShellRequest = ExitShellRequest.NORMAL_EXIT;
			}
			shell.waitForComplete();
		}
		catch(RuntimeException t){
			throw t;
		}
		finally {
			HandlerUtils.flushAllHandlers(java.util.logging.Logger.getLogger(""));
		}

		System.exit(exitShellRequest.getExitCode());
	}
	
	public void displayErrorMessage(final String message, final Throwable error)
	{
		System.err.println(String.format("Error: %s", message));
		log.error("{}: {}", new Object[]{message, error.getMessage(), error});
	}
	
	public static void displayStandaloneErrorMessage(final String message, final Throwable error)
	{
		System.err.println(String.format("Error: %s", message));
		log.error("{}: {}", new Object[]{message, error.getMessage(), error});
	}
}
