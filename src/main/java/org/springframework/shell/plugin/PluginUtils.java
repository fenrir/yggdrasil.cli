/*
 * Copyright 2011-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.shell.plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.google.inject.Key;
import com.google.inject.TypeLiteral;

import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.fenrir.yggdrasil.cli.plugin.BannerProvider;
import org.fenrir.yggdrasil.cli.plugin.HistoryFileNameProvider;
import org.fenrir.yggdrasil.cli.plugin.NamedProvider;
import org.fenrir.yggdrasil.cli.plugin.PromptProvider;
import org.fenrir.yggdrasil.core.ApplicationContext;

/**
 * Utilities dealing with shell plugins.
 *
 * @author Erwin Vervaet
 */
public final class PluginUtils 
{
	private PluginUtils() 
	{
		
	}

	public static BannerProvider getHighestPriorityBannerProvider()
	{
		TypeLiteral<Set<BannerProvider>> typeLiteral = new TypeLiteral<Set<BannerProvider>>(){};
		Key<Set<BannerProvider>> key = Key.get(typeLiteral);
		Set<BannerProvider> providers = (Set<BannerProvider>)ApplicationContext.getInstance().getRegisteredComponent(key);
		return getHighestPriorityProvider(providers);
	}
	
	public static PromptProvider getHighestPriorityPromptProvider()
	{
		TypeLiteral<Set<PromptProvider>> typeLiteral = new TypeLiteral<Set<PromptProvider>>(){};
		Key<Set<PromptProvider>> key = Key.get(typeLiteral);
		Set<PromptProvider> providers = (Set<PromptProvider>)ApplicationContext.getInstance().getRegisteredComponent(key);
		return getHighestPriorityProvider(providers);
	}
	
	public static HistoryFileNameProvider getHighestPriorityHistoryFilenameProvider()
	{
		TypeLiteral<Set<HistoryFileNameProvider>> typeLiteral = new TypeLiteral<Set<HistoryFileNameProvider>>(){};
		Key<Set<HistoryFileNameProvider>> key = Key.get(typeLiteral);
		Set<HistoryFileNameProvider> providers = (Set<HistoryFileNameProvider>)ApplicationContext.getInstance().getRegisteredComponent(key);
		return getHighestPriorityProvider(providers);
	}
	
	private static <T extends NamedProvider> T getHighestPriorityProvider(Set<T> providers)
	{
		List<T> sortedProviders = new ArrayList<T>(providers);
		Collections.sort(sortedProviders, new AnnotationAwareOrderComparator());
		T highestPriorityProvider = sortedProviders.get(0);
		
		return highestPriorityProvider;
	}
}
