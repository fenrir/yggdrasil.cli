/*
 * Copyright 2011-2012 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.shell.core;

import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.fenrir.yggdrasil.cli.commands.CommandMarker;
import org.fenrir.yggdrasil.cli.plugin.BannerProvider;
import org.fenrir.yggdrasil.cli.plugin.HistoryFileNameProvider;
import org.fenrir.yggdrasil.cli.plugin.PromptProvider;
import org.springframework.shell.CommandLine;
import org.springframework.shell.plugin.PluginUtils;

/**
 * Launcher for {@link JLineShell}.
 *
 * @author Ben Alex
 * @since 1.1
 */
public class JLineShellComponent extends JLineShell
{
	private CommandLine commandLine;
	
	private volatile boolean running = false;
	private Thread shellThread;

	private boolean printBanner = true;

	private String historyFileName;
	private String promptText;
	private String productName;
	private String banner;
	private String version;
	private String welcomeMessage;

	private ExecutionStrategy executionStrategy = new SimpleExecutionStrategy();
	private SimpleParser parser = new SimpleParser();
	
	@Inject
	private Set<CommandMarker> commands;
	@Inject 
	private Set<Converter> converters;
	
	public void setCommands(Set<CommandMarker> commands)
	{
		this.commands = commands;
	}
	
	public void setConverters(Set<Converter> converters)
	{
		this.converters = converters;
	}

	public void setCommandLine(CommandLine commandLine)
	{
		this.commandLine = commandLine;
		setHistorySize(commandLine.getHistorySize());
		if (commandLine.getShellCommandsToExecute() != null) {
			setPrintBanner(false);
		}
	}
	
	public SimpleParser getSimpleParser() {
		return parser;
	}

	public boolean isAutoStartup() {
		return false;
	}
	
	public void stop(Runnable callback) {
		stop();
		callback.run();
	}
	
	public int getPhase() {
		return 1;
	}
	
	public void start() {
		//customizePlug must run before start thread to take plugin's configuration into effect
		customizePlugin();
		shellThread = new Thread(this, "Spring Shell");
		shellThread.start();
		running = true;
	}


	public void stop() {
		if (running) {
			closeShell();
			running = false;
		}
	}

	public boolean isRunning() {
		return running;
	}
	
	@SuppressWarnings("rawtypes")
	@PostConstruct
	public void setUp()
	{
		for (CommandMarker command : commands) {
			getSimpleParser().add(command);
		}
		for (Converter<?> converter : converters) {
			getSimpleParser().add(converter);
		}
	}

	/**
	 * wait the shell command to complete by typing "quit" or "exit" 
	 * 
	 */
	public void waitForComplete() {
		try {
			shellThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected ExecutionStrategy getExecutionStrategy() {
		return executionStrategy;
	}

	@Override
	protected Parser getParser() {
		return parser;
	}

	@Override
	public String getStartupNotifications() {
		return null;
	}

	public void customizePlugin() {
		this.historyFileName = getHistoryFileName();
		this.promptText = getPromptText();
		String[] banner = getBannerText();
		this.banner = banner[0];
		this.welcomeMessage = banner[1];
		this.version = banner[2];
		this.productName = banner[3];
	}

	/**
	 * get history file name from provider. The provider has highest order 
	 * <link>org.springframework.core.Ordered.getOder</link> will win. 
	 * 
	 * @return history file name 
	 */
	protected String getHistoryFileName() {
		HistoryFileNameProvider historyFileNameProvider = PluginUtils.getHighestPriorityHistoryFilenameProvider();
		String providerHistoryFileName = historyFileNameProvider.getHistoryFileName();
		if (providerHistoryFileName != null) {
			return providerHistoryFileName;
		} else {
			return historyFileName;
		}
	}

	/**
	 * get prompt text from provider. The provider has highest order 
	 * <link>org.springframework.core.Ordered.getOder</link> will win. 
	 * 
	 * @return prompt text
	 */
	protected String getPromptText() 
	{
		PromptProvider promptProvider = PluginUtils.getHighestPriorityPromptProvider();
		String providerPromptText = promptProvider.getPrompt();
		if (providerPromptText != null) {
			return providerPromptText;
		} else {
			return promptText;
		}
	}

	/**
	 * Get Banner and Welcome Message from provider. The provider has highest order 
	 * <link>org.springframework.core.Ordered.getOder</link> will win. 
	 * @return BannerText[0]: Banner
	 *         BannerText[1]: Welcome Message
	 *         BannerText[2]: Version
	 *         BannerText[3]: Product Name
	 */
	private String[] getBannerText() 
	{
		String[] bannerText = new String[4];
		BannerProvider provider = PluginUtils.getHighestPriorityBannerProvider();
		bannerText[0] = provider.getBanner();
		bannerText[1] = provider.getWelcomeMessage();
		bannerText[2] = provider.getVersion();
		bannerText[3] = provider.getProviderName();
		return bannerText;
	}

	
	public void printBannerAndWelcome() {
	    if (printBanner) {
			logger.info(this.banner);
			logger.info(getWelcomeMessage());
		}
	}


	/**
	 * get the welcome message at start.
	 * 
	 * @return welcome message
	 */
	public String getWelcomeMessage() {
		return this.welcomeMessage;
	}


	/**
	 * @param printBanner the printBanner to set
	 */
	public void setPrintBanner(boolean printBanner) {
		this.printBanner = printBanner;
	}
	
	protected String getProductName() {
		return productName;
	}
	
	protected String getVersion() {
		return version;
	}
}