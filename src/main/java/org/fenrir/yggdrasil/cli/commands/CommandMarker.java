package org.fenrir.yggdrasil.cli.commands;

/**
 * Marker interface indicating a provider of one or more shell commands.
 * @version 0.1.20141230
 */
public interface CommandMarker 
{
	
}
