package org.fenrir.yggdrasil.cli;

import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.extension.IWindowManagementService;
import org.springframework.shell.ShellManager;

public class WindowManagementServiceProvider implements IWindowManagementService 
{
	@Override
	public void initialize(AbstractApplication application) 
	{
		ShellManager.getInstance().initialize(application);
	}
	
	@Override
	public void notifyPreWindowOpenEvent() 
	{
		// Res a fer...
	}
	
	@Override
	public void displayErrorMessage(String message, Throwable error) 
	{
		ShellManager.getInstance().displayErrorMessage(message, error);
	}

	@Override
	public void displayStandaloneErrorMessage(String message, Throwable error) 
	{
		ShellManager.displayStandaloneErrorMessage(message, error);
	}
	
	@Override
	public void createWindow() throws ApplicationException 
	{
		ShellManager.getInstance().createShell();
	}

	@Override
	public void showWindow() 
	{
		ShellManager.getInstance().runShell();
	}
}
