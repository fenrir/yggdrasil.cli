package org.fenrir.yggdrasil.cli.module;

import javax.inject.Singleton;

import org.fenrir.yggdrasil.cli.plugin.BannerProvider;
import org.fenrir.yggdrasil.cli.plugin.HistoryFileNameProvider;
import org.fenrir.yggdrasil.cli.plugin.PromptProvider;
import org.springframework.shell.plugin.support.DefaultBannerProvider;
import org.springframework.shell.plugin.support.DefaultHistoryFileNameProvider;
import org.springframework.shell.plugin.support.DefaultPromptProvider;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

public class ProvidersModule extends AbstractModule 
{
	@Override
	protected void configure() 
	{
		Multibinder<BannerProvider> bannerProviderBinder = Multibinder.newSetBinder(binder(), BannerProvider.class);
		bannerProviderBinder.addBinding().to(DefaultBannerProvider.class).in(Singleton.class);
		
		Multibinder<PromptProvider> promptProviderBinder = Multibinder.newSetBinder(binder(), PromptProvider.class);
		promptProviderBinder.addBinding().to(DefaultPromptProvider.class).in(Singleton.class);
		
		Multibinder<HistoryFileNameProvider> historyProviderBinder = Multibinder.newSetBinder(binder(), HistoryFileNameProvider.class);
		historyProviderBinder.addBinding().to(DefaultHistoryFileNameProvider.class).in(Singleton.class);
	}
}
