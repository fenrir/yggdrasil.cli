package org.fenrir.yggdrasil.cli.module;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

import org.fenrir.yggdrasil.cli.commands.CommandMarker;
import org.springframework.shell.commands.ConsoleCommands;
import org.springframework.shell.commands.DateCommands;
import org.springframework.shell.commands.ExitCommands;
import org.springframework.shell.commands.HelpCommands;
import org.springframework.shell.commands.InlineCommentCommands;
import org.springframework.shell.commands.OsCommands;
import org.springframework.shell.commands.OsOperations;
import org.springframework.shell.commands.OsOperationsImpl;
import org.springframework.shell.commands.ScriptCommands;
import org.springframework.shell.commands.SystemPropertyCommands;
import org.springframework.shell.commands.VersionCommands;

public class CommandsModule extends AbstractModule 
{
	@Override
	protected void configure() 
	{
		Multibinder<CommandMarker> commandBinder = Multibinder.newSetBinder(binder(), CommandMarker.class);
		commandBinder.addBinding().to(ConsoleCommands.class).in(Singleton.class);
		commandBinder.addBinding().to(DateCommands.class).in(Singleton.class);
		commandBinder.addBinding().to(ExitCommands.class).in(Singleton.class);
		commandBinder.addBinding().to(InlineCommentCommands.class).in(Singleton.class);
		commandBinder.addBinding().to(SystemPropertyCommands.class).in(Singleton.class);
		commandBinder.addBinding().to(OsCommands.class).in(Singleton.class);
		commandBinder.addBinding().to(HelpCommands.class).in(Singleton.class);
		commandBinder.addBinding().to(ScriptCommands.class).in(Singleton.class);
		commandBinder.addBinding().to(VersionCommands.class).in(Singleton.class);

		bind(OsOperations.class).to(OsOperationsImpl.class).in(Singleton.class);
	}
}
