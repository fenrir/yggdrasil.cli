package org.fenrir.yggdrasil.cli.module;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Locale;

import javax.inject.Singleton;

import org.fenrir.yggdrasil.cli.commands.CommandMarker;
import org.springframework.shell.commands.ConsoleCommands;
import org.springframework.shell.converters.BigDecimalConverter;
import org.springframework.shell.converters.BigIntegerConverter;
import org.springframework.shell.converters.BooleanConverter;
import org.springframework.shell.converters.CharacterConverter;
import org.springframework.shell.converters.DateConverter;
import org.springframework.shell.converters.DoubleConverter;
import org.springframework.shell.converters.EnumConverter;
import org.springframework.shell.converters.FileConverter;
import org.springframework.shell.converters.FloatConverter;
import org.springframework.shell.converters.IntegerConverter;
import org.springframework.shell.converters.LocaleConverter;
import org.springframework.shell.converters.LongConverter;
import org.springframework.shell.converters.ShortConverter;
import org.springframework.shell.converters.SimpleFileConverter;
import org.springframework.shell.converters.StaticFieldConverter;
import org.springframework.shell.converters.StaticFieldConverterImpl;
import org.springframework.shell.converters.StringConverter;
import org.springframework.shell.core.Converter;
import org.springframework.shell.core.JLineShellComponent;
import org.springframework.shell.core.Shell;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;

public class ShellModule extends AbstractModule 
{
	@Override
	protected void configure() 
	{
		bind(Shell.class).to(JLineShellComponent.class).in(Singleton.class);
		
//		bind(new TypeLiteral<Converter<Short>>(){}).to(ShortConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<Integer>>(){}).to(IntegerConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<Long>>(){}).to(LongConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<BigInteger>>(){}).to(BigIntegerConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<Float>>(){}).to(FloatConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<Double>>(){}).to(DoubleConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<BigDecimal>>(){}).to(BigDecimalConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<Character>>(){}).to(CharacterConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<String>>(){}).to(StringConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<Boolean>>(){}).to(BooleanConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<Date>>(){}).to(DateConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<Locale>>(){}).to(LocaleConverter.class).in(Singleton.class);
//		bind(new TypeLiteral<Converter<Enum<?>>>(){}).to(EnumConverter.class).in(Singleton.class);
//		
//		bind(StaticFieldConverter.class).to(StaticFieldConverterImpl.class).in(Singleton.class);
//		
//		bind(FileConverter.class).to(SimpleFileConverter.class).in(Singleton.class);		
		
		Multibinder<Converter> converterBinder = Multibinder.newSetBinder(binder(), Converter.class);
		converterBinder.addBinding().to(ShortConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(IntegerConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(LongConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(BigIntegerConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(FloatConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(DoubleConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(BigDecimalConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(CharacterConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(StringConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(BooleanConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(DateConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(LocaleConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(EnumConverter.class).in(Singleton.class);
		converterBinder.addBinding().to(StaticFieldConverterImpl.class).in(Singleton.class);
		converterBinder.addBinding().to(SimpleFileConverter.class).in(Singleton.class);
	}
}
