package org.fenrir.yggdrasil.cli.plugin;

/**
 * Shell prompt provider.
 * Plugins should implement this interface to customize prompt.
 * <code>getOrder</code> indicate the priority, higher values can be interpreted as lower priority
 * @since 0.1
 * @version 0.1.20141230
 */
public interface PromptProvider extends NamedProvider 
{
	/**
	 * Returns the prompt text.
	 * @return String
	 */
	String getPrompt();
}
