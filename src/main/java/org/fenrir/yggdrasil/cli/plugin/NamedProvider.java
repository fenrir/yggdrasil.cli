package org.fenrir.yggdrasil.cli.plugin;

/**
 * Returns the name of the provider.  Providers customize features of the shell such as the banner and command line prompt.
 * 
 * @since 0.1
 * @version 0.1.20141230
 */
public interface NamedProvider 
{
	/**
	 * Return the name of the provider.
	 */
	String getProviderName();
}
