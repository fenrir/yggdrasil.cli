package org.fenrir.yggdrasil.cli.plugin;

/**
 * History file name provider. 
 * Plugin should implement this interface to customize history file. 
 * <code>getOrder</code> indicate the priority, higher values can be interpreted as lower priority
 * @since 0.1
 * @version 0.1.20141230
 */
public interface HistoryFileNameProvider extends NamedProvider 
{
	/**
	 * Get history file name
	 * @return String - history file name
	 */
	String getHistoryFileName();
}
