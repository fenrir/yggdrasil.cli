package org.fenrir.yggdrasil.cli.plugin;

/**
 * Banner provider. Plugins should implement this interface to replace the version banner.
 * Use the @Order annotation to specify the priority of the banner to be display, higher 
 * values can be interpreted as lower priority
 * 
 * @since 0.1
 * @version 0.1.20141230
 */
public interface BannerProvider extends NamedProvider 
{
	/**
	 * Returns the banner.
	 * 
	 * @return
	 */
	String getBanner();

	/**
	 * Returns the associated version.
	 * 
	 * @return
	 */
	String getVersion();

	/**
	 * Returns the welcome message.
	 * 
	 * @return
	 */
	String getWelcomeMessage();
}
